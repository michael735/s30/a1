const express = require('express');
const app = express();
const PORT = 3007;
const dotenv = require('dotenv');
const mongoose = require('mongoose');
dotenv.config();

//Middlewares
app.use(express.json())
app.use(express.urlencoded({extended:true}))

//Mongoose connection
mongoose.connect(process.env.MONGO_URL,{useNewUrlParser: true, useUnifiedTopology: true});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to database`));


const signUpSchema = new mongoose.Schema(
    {
        username : {
            type: String,
            required: [true, `Username is required`]
        },
        password : {
            type: String,
            required: [true, `Password is required`]
        }
    }
)

const Signup = mongoose.model(`Signup`, signUpSchema)

app.post(`/signup`, (req, res) => {

    Signup.findOne({name: req.body.name}).then((result, err) => {
        console.log(result)
        if(result != null && result.username == req.body.username){
            return res.send(`Username is already used`)
        }else {
            let newUser = new Signup({
                username: req.body.username,
                password: req.body.password
            })
            newUser.save().then((userInfo, err) => {
                if(userInfo){
                    return res.status(200).send(`New user registered`)
                } else{
                    return res.status(500).send(err)
                }
            })
        }
    })
})  

app.get(`/signup`, (req,res) =>{

    Signup.find({}).then((data, err) =>{
         if(data){
             return res.status(200).send(data)
         } else{
             return res.status(500).send.json(err)
         }
    })
})

app.listen(PORT, () => console.log(`Server connected to port ${PORT}`))

